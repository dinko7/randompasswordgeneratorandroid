package com.example.randompassword.menu.settings

import android.os.Bundle
import android.widget.Toast
import androidx.preference.PreferenceFragmentCompat
import com.example.randompassword.R

/**
 * SettingsFragment is a fragment which contains app preferences.
 */
class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }

    override fun onDestroyView() {
        Toast.makeText(context,"Changes will take effect after application is restarted.", Toast.LENGTH_SHORT).show()
        super.onDestroyView()
    }
}
