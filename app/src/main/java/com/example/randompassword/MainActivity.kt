package com.example.randompassword

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.preference.PreferenceManager
import com.example.randompassword.generator.PasswordViewModel
import com.example.randompassword.utils.preferences.Themes
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        //set theme
        val variant = sharedPreferences.getBoolean(resources.getString(R.string.sp_key_dark_theme), false)
        val color = sharedPreferences.getString(resources.getString(R.string.sp_key_accent_color), Themes.Colors.ORANGE)
        Themes.getTheme(variant, color)?.let {
            setTheme(it)
        }

        setContentView(R.layout.activity_main)

        val navController = this.findNavController(R.id.myNavHostFragment)
        NavigationUI.setupActionBarWithNavController(this, navController)

    }

    override fun onSupportNavigateUp(): Boolean {
        return this.findNavController(R.id.myNavHostFragment).navigateUp()
    }
}
