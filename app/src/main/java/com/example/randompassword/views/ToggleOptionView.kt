package com.example.randompassword.views

import android.content.Context
import android.util.AttributeSet
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.randompassword.R

/**
 * ToggleOptionView is a custom [View] which is used to describe a toggle charset.
 */
class ToggleOptionView(context: Context, attrs: AttributeSet): ConstraintLayout(context, attrs) {

    var switch: Switch

    init {
        inflate(context, R.layout.toggle_option_view, this)

        val textView: TextView = findViewById(R.id.text)
        switch = findViewById(R.id.toggle)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.ToggleOptionView)
        textView.text = attributes.getString(R.styleable.ToggleOptionView_text)
        textView.setCompoundDrawablesWithIntrinsicBounds(attributes.getDrawable(R.styleable.ToggleOptionView_icon),null,null,null)
        attributes.recycle()

    }

    fun getSwitchValue() : Boolean {
        return switch.isChecked
    }

    fun setSwitchValue( value : Boolean) {
        switch.isChecked = value
    }

    fun setSwitchStateListener(listener: ((CompoundButton, Boolean) -> Unit)?) {
        switch.setOnCheckedChangeListener(listener)
    }

}