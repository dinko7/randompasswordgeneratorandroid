package com.example.randompassword.generator

/**
 * This object is used to map password strength to its according description and color
 * to update the UI.
 */
object PasswordStrengthMeter {

    object Score {
        const val UNDEFINED = -1
        const val WEAK = 0
        const val FAIR = 1
        const val GOOD = 2
        const val STRONG = 3
        const val VERY_STRONG = 4
    }

    private object Description {
        const val UNDEFINED = ""
        const val WEAK = "Weak"
        const val FAIR = "Fair"
        const val GOOD = "Good"
        const val STRONG = "Strong"
        const val VERY_STRONG = "Very Strong"
    }

    private object Colors {
        const val UNDEFINED = android.R.color.white
        const val WEAK = android.R.color.holo_red_dark
        const val FAIR = android.R.color.holo_orange_dark
        const val GOOD = android.R.color.holo_orange_light
        const val STRONG = android.R.color.holo_green_light
        const val VERY_STRONG = android.R.color.holo_green_dark
    }

    private val scoreToStringMap
            = mapOf(
        Score.UNDEFINED to Description.UNDEFINED,
        Score.WEAK to Description.WEAK,
        Score.FAIR to Description.FAIR,
        Score.GOOD to Description.GOOD,
        Score.STRONG to Description.STRONG,
        Score.VERY_STRONG to Description.VERY_STRONG
    )

    private val scoreToColorMap
            = mapOf(
        Score.UNDEFINED to Colors.UNDEFINED,
        Score.WEAK to Colors.WEAK,
        Score.FAIR to Colors.FAIR,
        Score.GOOD to Colors.GOOD,
        Score.STRONG to Colors.STRONG,
        Score.VERY_STRONG to Colors.VERY_STRONG
    )

    /**
     * Returns string which is a description of a particular score.
     */
    fun getScoreDescription(score : Int) : String {
        return scoreToStringMap[score] ?: return Description.UNDEFINED
    }

    /**
     * Returns a color for particular score.
     */
    fun getScoreColor(score : Int) : Int {
        return scoreToColorMap[score] ?: return Colors.UNDEFINED
    }
}