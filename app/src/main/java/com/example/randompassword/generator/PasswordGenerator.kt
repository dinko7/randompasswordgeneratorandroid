package com.example.randompassword.generator

import timber.log.Timber
import java.security.SecureRandom


/**
 * PasswordGenerator is an object used to generate random password.
 */
object PasswordGenerator {

    object Constants {
        const val minPasswordLength = 8
        const val maxPasswordLength = 19
        const val defaultPasswordLength = 10
    }

    private val secureRandom = SecureRandom()

    /**
     * collectCharsets is a method which collects and merges selected charsets into a string.
     */
    fun collectCharsets(lowercase : Boolean, uppercase : Boolean, numbers : Boolean, symbols: Boolean) : String {

        Timber.i(lowercase.toString() + " " + uppercase.toString() + " " + numbers.toString() + " " + symbols.toString())

        var stringBuilder: StringBuilder = StringBuilder()

        if (lowercase) stringBuilder.append(Charsets.lowerCase)
        if (uppercase) stringBuilder.append(Charsets.upperCase)
        if (numbers) stringBuilder.append(Charsets.numbers)
        if (symbols) stringBuilder.append(Charsets.symbols)

        return stringBuilder.toString()
    }

    /**
     * Method used to generate password.
     */
    fun generate(passwordLength : Int, charset : String) : String {
        Timber.i(passwordLength.toString() + ", charset: " + charset)
        var passwordStringBuilder: StringBuilder = StringBuilder()

        for (x in 0 until passwordLength) {
            var nextChar = charset[secureRandom.nextInt(charset.length)]

            while (passwordStringBuilder.contains(nextChar, true)) {
                nextChar = charset[secureRandom.nextInt(charset.length)]
            }
            passwordStringBuilder.append(nextChar)
        }

        Timber.i(passwordStringBuilder.toString())
        return passwordStringBuilder.toString()
    }
}