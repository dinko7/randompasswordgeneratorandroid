package com.example.randompassword.generator

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * PasswordViewModel is [ViewModel] which is used to update GeneratorFragments
 * password area and password length.
 */
class PasswordViewModel : ViewModel() {

    private val _password = MutableLiveData<String>()
    val password : LiveData<String>
            get() = _password

    private val _passwordLength = MutableLiveData<Int>()
    val passwordLength : LiveData<Int>
        get() = _passwordLength

    private val _passwordStrength = MutableLiveData<Int>()
    val passwordStrength : LiveData<Int>
        get() = _passwordStrength

    init {
        setPassword("")
        setPasswordLength(PasswordGenerator.Constants.defaultPasswordLength)
        setPasswordStrength(PasswordStrengthMeter.Score.UNDEFINED)
    }

    fun setPassword(password : String) {
        _password.value = password
    }

    fun setPasswordLength(length : Int) {
        _passwordLength.value = length
    }

    fun increasePasswordLength() : Boolean {
        return if (_passwordLength.value!! >= PasswordGenerator.Constants.maxPasswordLength) {
            false
        } else {
            _passwordLength.value = _passwordLength.value!! +1
            true
        }
    }

    fun decreasePasswordLength() : Boolean {
        return if (_passwordLength.value!! <= PasswordGenerator.Constants.minPasswordLength) {
            false
        } else {
            _passwordLength.value = _passwordLength.value!! -1
            true
        }
    }

    fun setPasswordStrength(strength : Int) {
        _passwordStrength.value = strength
    }

}