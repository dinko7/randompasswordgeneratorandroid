package com.example.randompassword.generator

import android.content.ClipData
import android.content.ClipboardManager
import android.content.SharedPreferences
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import com.example.randompassword.R
import com.example.randompassword.databinding.FragmentGeneratorBinding
import com.nulabinc.zxcvbn.Strength
import com.nulabinc.zxcvbn.Zxcvbn


/**
 * Main fragment used to select which characters to use to generate password
 * and generate password.
 */
class GeneratorFragment : Fragment() {

    private lateinit var binding: FragmentGeneratorBinding
    private lateinit var viewModel: PasswordViewModel
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var zxcvbnPasswordUtil : Zxcvbn

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //inflate layout with data binding utils
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_generator, container, false )

        //shared prefrences
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.activity)

        //password strength meter
        zxcvbnPasswordUtil = Zxcvbn()

        //setup viewmodel
        viewModel = ViewModelProviders.of(this).get(PasswordViewModel::class.java)
        //restore password length from previous session
        viewModel.setPasswordLength(sharedPreferences.getInt(resources.getString(R.string.sp_key_password_length), PasswordGenerator.Constants.defaultPasswordLength))
        binding.passwordViewModel = viewModel
        binding.passwordStrengthMeter = PasswordStrengthMeter
        binding.setLifecycleOwner(this)

        //setup buttons
        val btnGenerateDrawable = resources.getDrawable(R.drawable.generate_button)
        binding.btnGenerate.setBackgroundDrawable(setTint(btnGenerateDrawable, getAccentColor()))
        binding.btnGenerate.setOnClickListener { generatePassword() }
        binding.btnCopy.setOnClickListener { copyPasswordToClipboard() }
        binding.btnIncrease.setOnClickListener { increasePasswordLength() }
        binding.btnDecrease.setOnClickListener { decreasePasswordLength() }


        //enable menu
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onStart() {

        //setup switches
        val lowercase: Boolean = sharedPreferences.getBoolean(resources.getString(R.string.sp_key_lowercase), true)
        binding.lowercase.setSwitchValue(lowercase)
        binding.lowercase.setSwitchStateListener { _, isChanged -> SwitchStatePreferenceListener(resources.getString(R.string.sp_key_lowercase), isChanged)}

        val uppercase: Boolean = sharedPreferences.getBoolean(resources.getString(R.string.sp_key_uppercase), false)
        binding.uppercase.setSwitchValue(uppercase)
        binding.uppercase.setSwitchStateListener { _, isChanged -> SwitchStatePreferenceListener(resources.getString(R.string.sp_key_uppercase), isChanged)}

        val numbers: Boolean = sharedPreferences.getBoolean(resources.getString(R.string.sp_key_numbers), false)
        binding.numbers.setSwitchValue(numbers)
        binding.numbers.setSwitchStateListener { _, isChanged -> SwitchStatePreferenceListener(resources.getString(R.string.sp_key_numbers), isChanged)}

        val symbols: Boolean = sharedPreferences.getBoolean(resources.getString(R.string.sp_key_symbols), false)
        binding.symbols.setSwitchValue(symbols)
        binding.symbols.setSwitchStateListener { _, isChanged -> SwitchStatePreferenceListener(resources.getString(R.string.sp_key_symbols), isChanged)}

        super.onStart()
    }

    override fun onPause() {
        //save password length
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putInt(resources.getString(R.string.sp_key_password_length),
            viewModel.passwordLength.value!!
        )
        editor.apply()
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        //inflate menu
        inflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //hand menu navigation to other fragments
        if (item.itemId == R.id.settings ) {
            this.findNavController().navigate(R.id.action_generatorFragment_to_settingsFragment)
        } else if (item.itemId == R.id.about) {
            this.findNavController().navigate(R.id.action_generatorFragment_to_aboutFragment)
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Method used to generate password.
     * First collect and merge all selected charsets, then pass it
     * to the generator along with password length.
     */
    private fun generatePassword() {

        //collect charsets
        val charset = PasswordGenerator.collectCharsets(
            binding.lowercase.getSwitchValue(),
            binding.uppercase.getSwitchValue(),
            binding.numbers.getSwitchValue(),
            binding.symbols.getSwitchValue()
        )

        //generate password
        val password = PasswordGenerator.generate(viewModel.passwordLength.value!!, charset)

        //set password
        viewModel.setPassword(password)
        val strength = zxcvbnPasswordUtil.measure(password)
        viewModel.setPasswordStrength(strength.score)

    }

    /**
     * SwitchStatePreferenceListener is a method used to save switch state to
     * [SharedPreferences] when is changes.
     */
    private fun SwitchStatePreferenceListener(key : String, value : Boolean) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    /**
     * Sets color(tint) of a [Drawable] object.
     */
    fun setTint(d: Drawable?, color: Int): Drawable? {
        val wrappedDrawable = DrawableCompat.wrap(d!!)
        DrawableCompat.setTint(wrappedDrawable, color)
        return wrappedDrawable
    }

    /**
     * Returns colorAccent of a currently set theme.
     */
    private fun getAccentColor(): Int {
        val typedValue = TypedValue()
        val a: TypedArray =
            requireContext().obtainStyledAttributes(typedValue.data, intArrayOf(R.attr.colorAccent))
        val color = a.getColor(0, 0)
        a.recycle()
        return color
    }

    /**
     * Copies password to the clipboard and displays toast message.
     */
    private fun copyPasswordToClipboard() {
        val clipboard = ContextCompat.getSystemService(this.requireContext(), ClipboardManager::class.java)
        val clip = ClipData.newPlainText("text", viewModel.password.value)
        clipboard?.setPrimaryClip(clip)
        Toast.makeText(context,"Password copied.", Toast.LENGTH_SHORT).show()
    }

    /**
     * Decreases password length by 1, and displays toast if password length would be too small.
     */
    private fun decreasePasswordLength() {
        if (!viewModel.decreasePasswordLength()) {
            Toast.makeText(context,"Password length cannot be less then " + PasswordGenerator.Constants.minPasswordLength.toString() +".", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Increases password length by 1, and displays toast if password length would be too big.
     */
    private fun increasePasswordLength() {
        if (!viewModel.increasePasswordLength()) {
            Toast.makeText(context,"Password length cannot be more then " + PasswordGenerator.Constants.maxPasswordLength.toString() +".", Toast.LENGTH_SHORT).show()
        }
    }

}
