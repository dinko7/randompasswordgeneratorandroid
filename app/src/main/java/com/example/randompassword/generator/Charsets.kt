package com.example.randompassword.generator

/**
 * Object used to define charsets.
 */
object Charsets {
    const val upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    const val lowerCase = "abcdefghijklmnopqrstuvwxyz"
    const val numbers = "0123456789"
    const val symbols = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
}