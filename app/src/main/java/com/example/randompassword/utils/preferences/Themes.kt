package com.example.randompassword.utils.preferences

import com.example.randompassword.R
import timber.log.Timber

/**
 * Themes object is used to theme variant(light/dark) and theme color
 * to their respective styles.
 */
object Themes  {
    object Variant {
        const val LIGHT = false
        const val DARK = true
    }

    object Colors {
        const val RED = "COLOR_RED"
        const val GREEN = "COLOR_GREEN"
        const val BLUE = "COLOR_BLUE"
        const val BLUE2 = "COLOR_BLUE2"
        const val ORANGE = "COLOR_ORANGE"
        const val YELLOW = "COLOR_YELLOW"
        const val PURPLE = "COLOR_PURPLE"
        const val PINK = "COLOR_PINK"
    }

    private object Light {
        const val RED = R.style.MyRedLightTheme
        const val GREEN = R.style.MyGreenLightTheme
        const val BLUE = R.style.MyBlueLightTheme
        const val BLUE2 = R.style.MyBlue2LightTheme
        const val ORANGE = R.style.MyOrangeLightTheme
        const val YELLOW = R.style.MyYellowLightTheme
        const val PURPLE = R.style.MyPurpleLightTheme
        const val PINK = R.style.MyPinkLightTheme
    }

    private object Dark {
        const val RED = R.style.MyRedDarkTheme
        const val GREEN = R.style.MyGreenDarkTheme
        const val BLUE = R.style.MyBlueDarkTheme
        const val BLUE2 = R.style.MyBlue2DarkTheme
        const val ORANGE = R.style.MyOrangeDarkTheme
        const val YELLOW = R.style.MyYellowDarkTheme
        const val PURPLE = R.style.MyPurpleDarkTheme
        const val PINK = R.style.MyPinkDarkTheme
    }

    private val darkMap
            = mapOf(
        Colors.RED to Dark.RED,
        Colors.GREEN to Dark.GREEN,
        Colors.BLUE to Dark.BLUE,
        Colors.BLUE2 to Dark.BLUE2,
        Colors.ORANGE to Dark.ORANGE,
        Colors.YELLOW to Dark.YELLOW,
        Colors.PURPLE to Dark.PURPLE,
        Colors.PINK to Dark.PINK
    )

    private val lightMap
            = mapOf(
        Colors.RED to Light.RED,
        Colors.GREEN to Light.GREEN,
        Colors.BLUE to Light.BLUE,
        Colors.BLUE2 to Light.BLUE2,
        Colors.ORANGE to Light.ORANGE,
        Colors.YELLOW to Light.YELLOW,
        Colors.PURPLE to Light.PURPLE,
        Colors.PINK to Light.PINK
    )

    private val defaultTheme = darkMap[Colors.ORANGE]

    /**
     * Returns style according to the input parameteres.
     */
    fun getTheme(variant : Boolean?, color : String?) : Int? {
        Timber.i("getTheme => dark theme: " + variant.toString() + " ,color: " + color )
        if (variant != null) {
            if (variant == Variant.LIGHT) {
                Timber.i("getTheme: Light variant")
                return lightMap[color]
            } else if (variant == Variant.DARK) {
                Timber.i("getTheme: Dark variant")
                return darkMap[color]
            }
        }
        Timber.i("getTheme: Default value")
        return defaultTheme
    }
}