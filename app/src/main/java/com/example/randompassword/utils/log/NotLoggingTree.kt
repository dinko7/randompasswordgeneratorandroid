package com.example.randompassword.utils.log

import timber.log.Timber

/**
 * NotLoggingTree is a Timber tree which is used for example,
 * with release build so you dont have to manually remove all log statements.
 */
class NotLoggingTree : Timber.Tree() {
    override fun log(
        priority: Int,
        tag: String?,
        message: String,
        throwable: Throwable?
    ) {
    }
}
