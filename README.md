# RANDOM PASSWORD GENERATOR

**Random Password Generator** is a revival of my old high school project. Its purpose is to  demonstrate my Android development skills, and to learn some things in the process, of course.  Anyone is free to use this app as its fully functional. Just clone repo and build the app by using Android Studio.

App is designed to be very simple, customizable, compact and easy to use. Generate your password and get out, the app does the rest for you.


## App features
Here is a list of features:
- Select charsets you want to use to generate your password, there are 4 available: lowercase, uppercase, number and symbols
- Select length of your password
- Copy your password
- There is a password strength meter showing strength of every generated password so you can decide which level of security you want
- Choose between dark and ligth theme
- Choose main color
- The app saves your preferences(chosen charsets, password length, color) so you can get rigth back into action after closing the app

## Pictures of UI
![Main Screen](images/main_screen.jpg)
![Main Screen in landscape orientation with green ligth theme](images/main_screen_landscape.jpg)
![Settings Screen](images/settings.jpg)
![Choose a color setting](images/settings_color.jpg)
![Copy feature](images/copy.jpg)